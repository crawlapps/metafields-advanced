<?php

namespace App;

use App\Model\Group;
use Illuminate\Database\Eloquent\Model;

class MetafieldGroup extends Model
{
    public function groupData(){
        return $this->belongsTo(Group::class,'group_id','id')->with('configurations');
    }

    public function groupableMetafields(){
        return $this->hasMany(GroupableMetafield::class,'metafield_group_id','id')->with('configuration');
    }

}
