<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGroupIdToMetafields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metafield_configurations', function (Blueprint $table) {
            $table->unsignedBigInteger('group_id')->after('sort_order')->nullable();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metafield_configurations', function (Blueprint $table) {
            $table->dropColumn('group_id');
        });
    }
}
