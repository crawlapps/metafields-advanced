<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobIdToBackgroundActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('background_activities', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id')->nullable()->after('shop_id');
        });
        Schema::table('synced_details', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id')->nullable()->after('shop_id');
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('background_activities', function (Blueprint $table) {
            //
        });
    }
}
